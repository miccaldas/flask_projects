""" Module to insert notes to database """
from mysql.connector import connect, Error
import time
import click


def add():
    titulo = input(click.style(' Title? » ', fg='magenta', bold=True))
    tags = input(click.style(' Choose a tag » ', fg='magenta', bold=True))
    print(click.style(' Write the text.', fg='magenta', bold=True))
    time.sleep(0.2)
    texto = click.edit()
    answers = [titulo, texto, tags]
    try:
        conn = connect(
                host="localhost",
                user="mic",
                password="xxxx",
                database="poetry")
        cur = conn.cursor()
        query = """ INSERT INTO posts (post_title, post_text, tags) VALUES (%s, %s, %s) """
        cur.execute(query, answers)
        conn.commit()
    except Error as e:
        print("Error while connecting to db", e)
    finally:
        if(conn):
            conn.close()


if __name__ == '__main__':
    add()
