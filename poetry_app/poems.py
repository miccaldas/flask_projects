from flask import Flask, render_template
from flask_bootstrap import Bootstrap
from flask_mail import Mail

app = Flask(__name__)
bootstrap = Bootstrap(app)
mail = Mail(app)

app.config['MAIL_SERVER'] = 'smtp.mailfence.com'
app.config['MAIL_PORT'] = 25
app.config['MAIL_USE_TLS'] = True
app.config['MAIL_USERNAME'] = 'micaldas@mailfence.com'
app.config['MAIL_PASSWORD'] = 'xxxx'


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/poems/arrow')
def arrow():
    return render_template('arrow.html')


@app.route('/poems/babes.html')
def babes():
    return render_template('babes.html')


@app.route('/poems/would.html')
def would():
    return render_template('would.html')


@app.route('/poems/travelling.html')
def travelling():
    return render_template('travelling.html')


@app.route('/poems/sense.html')
def sense():
    return render_template('sense.html')


@app.route('/poems/salt.html')
def salt():
    return render_template('salt.html')


@app.route('/poems/rain.html')
def rain():
    return render_template('rain.html')


@app.route('/poems/playground.html')
def playground():
    return render_template('playground.html')


@app.route('/poems/nosferatu.html')
def nosferatu():
    return render_template('nosferatu.html')


@app.route('/poems/mourners.html')
def mourners():
    return render_template('mourners.html')


@app.route('/poems/midday.html')
def midday():
    return render_template('midday.html')


@app.route('/poems/mario.html')
def mario():
    return render_template('mario.html')


@app.route('/poems/maria.html')
def maria():
    return render_template('maria.html')


@app.route('/poems/love.html')
def love():
    return render_template('love.html')


@app.route('/poems/light.html')
def light():
    return render_template('light.html')


@app.route('/poems/lazy.html')
def lazy():
    return render_template('lazy.html')


@app.route('/poems/inward.html')
def inward():
    return render_template('inward.html')


@app.route('/poems/ignorance.html')
def ignorance():
    return render_template('ignorance.html')


@app.route('/poems/ice.html')
def ice():
    return render_template('ice.html')


@app.route('/poems/grandmother.html')
def grandmother():
    return render_template('grandmother.html')


@app.route('/poems/glass.html')
def glass():
    return render_template('glass.html')


@app.route('/poems/friends.html')
def friends():
    return render_template('friends.html')


@app.route('/poems/freedom.html')
def freedom():
    return render_template('freedom.html')


@app.route('/poems/formal.html')
def formal():
    return render_template('formal.html')


@app.route('/poems/eumeswil.html')
def eumeswil():
    return render_template('eumeswil.html')


@app.route('/poems/cat.html')
def cat():
    return render_template('cat.html')


@app.route('/poems/cassandra.html')
def cassandra():
    return render_template('cassandra.html')


@app.route('/poems/black_girls.html')
def black():
    return render_template('black_girls.html')


@app.route('/poems/benzos.html')
def benzos():
    return render_template('benzos')