Flask_SQLAlchemy==2.5.1
Flask_Moment==0.11.0
Flask_WTF==0.14.3
WTForms==2.3.3
Flask_Migrate==2.7.0
Flask_Bootstrap==3.3.7.1
Flask==1.1.2
