import os
from datetime import datetime
from flask import Flask, render_template, session, redirect, url_for, flash
from flask_bootstrap import Bootstrap       # https://tinyurl.com/hkc3cf6
from flask_moment import Moment             # https://tinyurl.com/y3cu6h8t
from flask_wtf import FlaskForm             # https://tinyurl.com/h4mps9u
from flask_sqlalchemy import SQLAlchemy     # https://tinyurl.com/tsxfzsx
from flask_migrate import Migrate           # https://tinyurl.com/y6pvu6ep
from wtforms import TextAreaField, StringField, SubmitField
from wtforms.validators import Optional

basedir = os.path.abspath(os.path.dirname(__file__))

app = Flask(__name__)
bootstrap = Bootstrap(app)
moment = Moment(app)
app.config['SECRET_KEY'] = '62y*iç-xczYKr&"P8T/_uWD%me7tq`!5$}X.vE|S0wM'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'data.sqlite')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)
migrate = Migrate(app, db)


@app.shell_context_processor
def make_shell_context():
    return dict(db=db, User=User, Role=Role)


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(64), unique=True)

    def __repr__(self):
        return '<Role %r>' % self.name

    users = db.relationship('User', backref='role', lazy='dynamic')


class User(db.Model):
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), unique=True, index=True)

    def __repr__(self):
        return '<User %r>' % self.username

    role_id = db.Column(db.Integer, db.ForeignKey('roles.id'))


class BothForm(FlaskForm):
    name = StringField('What is your name?', validators=[Optional()])
    comment = TextAreaField('Comment here: ', validators=[Optional()])
    submit = SubmitField('Submit')


@app.route('/', methods=['GET', 'POST'])
def index():
    form = BothForm()
    if form.validate_on_submit():
        old_name = session.get('name')
        user = User.query.filter_by(username=form.name.data).first()
        if user is None:
            user = User(username=form.name.data)
            db.session.add(user)
            db.session.commit()
            session['known'] = False
        else:
            session['known'] = True
        session['name'] = form.name.data
        form.name.data = ''
        if old_name is not None and old_name != form.name.data:
            flash('It seems that you changed your name.')
        session['comment'] = form.comment.data
        return redirect(url_for('index'))
    return render_template('index.html', form=form, name=session.get('name'), known=session.get('known', False), comment=session.get('comment'), current_time=datetime.utcnow())


@app.route('/user/<name>')
def user(name):
    name = 'micaldas'
    title = 'user'
    return render_template('user.html', name=name, title=title)


@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_server_error(e):
    return render_template('500.html'), 500
